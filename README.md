Treviz Website
========================

![Treivz logo](./assets/images/treviz-banner.png)

## Description

Treviz is an open-source web platform that helps you set up and join open, collaborative organizations, in which you 
can work on the projects you want and get rewarded for your work.

This repository contains the code of the [Treviz website](https://treviz.xyz). It is built with 
[Symfony 4](http://symfony.com/) and is based on the Photon template by [HTML5 UP](http://html5up.net/).

## Requirements

* PHP 7.2 or higher.
* [Composer](http://getcomposer.org/)
* [NodeJS with NPM](https://nodejs.org)

## Set up

- Install the dependencies with Composer:
  ```
  composer install
  ```

- Specify the parameters of the database you want to use. 
  Once the connection parameters are setup, create the database and make the 
  appropriate migrations:
  ```
  php bin/console doctrine:database:create
  php bin/console doctrine:schema:update --force
  ```

- Install the front-end dependencies with npm:
  ```
  npm install
  ```

- Dump the assets using Encore:
  ```
  node_modules/.bin/encore <environment>
  ```
  The generated assets are located in the `public/build` directory.
  
  If you want to test the website on your local, developer environment, the
  environment is `dev`. Specify the `--watch` flag, so that the assets are
  updated each time you make a change in the `/assets` directory.
  
  If you want to deploy the website to your server, the environment is simply
  `production`

- Launch the web server. On your local machine, you can simply use the built-in 
  php web server:
  ```
  php -S 127.0.0.1:8000 -t public
  ```
  Browse the `127.0.0.1:8000` url; you should see the website correctly displayed.
  
  On a production environment, do not use the php web server; use a real one like
  [NGinx](http://nginx.org/) or [Apache](http://httpd.apache.org/). Virtual Host
  configuration suggestions are located in the `webserver-conf` folder.
  We recommend you secure your website using HTTPS. Check out [Let's Encrypt](http://letsencrypt.org/)
  to see how you can do it for free in a few clicks.

## Contributing

Check out our [Contributing Guide](./CONTRIBUTING.md) if you are interested in joining the development team, if you want
to submit a bug, etc.

## Contact
You can learn more about Treviz on:
* our website: [treviz.xyz](https://treviz.xyz)
* our documentation [doc.treviz.xyz](https://doc.treviz.xyz)
* our blog [blog.treviz.xyz](https://blog.treviz.xyz)

Feel free to [contact us](https://treviz.xyz/contact) if you have any question.
  
## To Do:
- Translate the website to French
- Write Legal Notices and Terms of Use
- Allow users to register and log in
- Create a dashboard to help them create and monitor their organizations
- Include payment solution