<?php

namespace App\Form;

use App\Entity\InstanceCreationRequest;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class InstanceCreationRequestType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array('label' => 'Name of your organization'))
            ->add('description', TextareaType::class, array('label' => 'Describe your organization\'s purpose'))
            ->add('adminFirstName', TextType::class, array('label' => 'Your email'))
            ->add('adminLastName', TextType::class, array('label' => 'Your first name'))
            ->add('email', TextType::class, array('label' => 'Your last name'))
            ->add('reference', CheckboxType::class, array('label' => 'Should your organization be listed in our list ?'))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // uncomment if you want to bind to a class
            'data_class' => InstanceCreationRequest::class,
        ]);
    }
}
