<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;

class FeaturesController extends Controller
{

    /**
     * @Route("/features/organization", name="features-organization")
     * @param Environment $twig
     * @return Response
     */
    public function organization(Environment $twig)
    {
        return new Response($twig->render('features/organization.html.twig'));
    }

    /**
     * @Route("/features/projects", name="features-projects")
     * @param Environment $twig
     * @return Response
     */
    public function projects(Environment $twig)
    {
        return new Response($twig->render('features/projects.html.twig'));
    }

    /**
     * @Route("/features/rewards", name="features-rewards")
     * @param Environment $twig
     * @return Response
     */
    public function rewards(Environment $twig)
    {
        return new Response($twig->render('features/rewards.html.twig'));
    }
}
