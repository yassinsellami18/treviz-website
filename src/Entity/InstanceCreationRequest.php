<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\InstanceCreationRequestRepository")
 */
class InstanceCreationRequest
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     * @Assert\Length(min=5,max=100)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $adminFirstName;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $adminLastName;

    /**
     * @var string
     * @ORM\Column(type="string")
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email.",
     *     checkMX = true
     * )
     */
    private $email;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $reference = false;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getAdminFirstName(): ?string
    {
        return $this->adminFirstName;
    }

    /**
     * @param string $adminFirstName
     */
    public function setAdminFirstName(string $adminFirstName)
    {
        $this->adminFirstName = $adminFirstName;
    }

    /**
     * @return string
     */
    public function getAdminLastName(): ?string
    {
        return $this->adminLastName;
    }

    /**
     * @param string $adminLastName
     */
    public function setAdminLastName(string $adminLastName)
    {
        $this->adminLastName = $adminLastName;
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email)
    {
        $this->email = $email;
    }

    /**
     * @return boolean
     */
    public function isReference(): ?bool
    {
        return $this->reference;
    }

    /**
     * @param boolean $reference
     */
    public function setReference(bool $reference)
    {
        $this->reference = $reference;
    }

}
